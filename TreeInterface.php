<?php

interface TreeInterface {

    public function build($params);

    public function getNodes();

    public function getParentId($id);

    public function setNodes($nodes);

    public function setRepo($repo);

    public function getRepo();

    public function getParentAndDescendants($parentId);

    public function getParentAndDescendantsFromList($parentIds);

    public function getChildrenIds($parentId);

    public function getIndirectIds($parentId);
    
    public function getTopParentId($parentId);

    public function getAncestorsIds($parentId, $includeTopParent);

    public function useMemcache($isActive = true);

}
