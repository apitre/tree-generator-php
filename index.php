<?php

error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', 1);

require('./Tree.php');
require('./TreeRepo.php');

$repo = new TreeRepo();

$tree = new Tree(false);

$tree->setRepo($repo);

$time_start = microtime(true);

$tree->build([]);

$time_end = microtime(true);
$time = $time_end - $time_start;

$serialized = serialize($tree->getNodes());

$size = mb_strlen($serialized, '8bit') / 1048576;

echo "\nBuild tree in ".($time * 1000)." milliseconds\n";
echo "Size of tree is $size mo\n\n";