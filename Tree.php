<?php

require('./TreeInterface.php');
require('./TreeNode.php');

class Tree implements TreeInterface
{
    private $repo = null;
    private $nodes = "";
    private $isLocal = false;

    public function __construct($isLocal)
    {
        $this->isLocal = $isLocal;
    }

    private function decodeIds($parentId)
    {
        $levels = explode("|", $this->nodes[$parentId]);

        return array_map(function($ids){
            return empty($ids) ? [] : explode(",", $ids);
        }, $levels);
    }

    public function getChildrenIds($parentId)
    {
        return $this->decodeIds($parentId)[1];
    }

    public function getIndirectIds($parentId)
    {
        return $this->decodeIds($parentId)[2];
    }

    public function setNodes($nodes)
    {
        $this->nodes = $nodes;
    }

    public function setRepo($repo)
    {
        $this->repo = $repo;
    }

    public function getRepo()
    {
        return $this->repo;
    }

    public function useMemcache($isActive = true)
    {
        //It is use on TreePortalProxy
    }

    public function getMemcacheKey($params, $version)
    {
        $this->repo->setParams($params);
        return $this->repo->getMemcacheKey($version);
    }

    public function getTopParentId($parentId)
    {
        $ancestorsIds = $this->getAncestorsIds($parentId, true);
        return end($ancestorsIds);
    }

    public function getParentId($id)
    {
        $i = (int)$id;

        if ($i < 1 || !isset($this->nodes[$i])) {
            return 0;
        }

        return (int)$this->decodeIds($i)[0][0];
    }

    public function getNodes()
    {
        return $this->nodes;
    }

    public function getAncestorsIds($parentId, $includeTopParent = true)
    {
        $ancestorsIds = $this->addParentId($parentId, [(int)$parentId]);

        if (!$includeTopParent) {
            array_pop($ancestorsIds);
        }

        return $ancestorsIds;
    }

    public function build($params)
    {
        $this->repo->setParams($params);

        $elements = $this->repo->getElements();

        $structure = $this->getParentChildrenStructure($elements);

        if (!empty($structure['errors'])) {
            var_dump($structure['errors']); exit;
        } else {
            $this->buildFromParents(0, null, $structure['rows'], $structure['children']);
        }

        //var_dump($this->nodes); exit;
    }

    protected function buildFromParents($i, $parent, $rows, $children)
    {
        $direct = "";
        $indirect = "";

        foreach ($children[$i] as $id) {
            
            if (isset($rows[$id])) {

                $direct .= "$id,";

                if (isset($children[$id])) {
                    $indirect .= $this->buildFromParents($id, $i, $rows, $children);
                } else {
                    $this->nodes .= "[$id]$i||";
                }

            }

        }

        $this->nodes .= "[$i]$parent|".rtrim($direct, ",")."|".rtrim($indirect, ",");

        return $indirect.$direct;
    }

    private function validData($rows, $u, $p)
    {
        if(isset($rows[$p]) && $rows[$p] == $u) {
            return "[id => {$p} parent_id => {$rows[$p]}] [id => {$u} parent_id => {$p}]";
        }

        if (isset($rows[$u])) {
            return "[id => {$u} parent_id => {$rows[$u]}] [id => {$u} parent_id => {$p}]";
        }

        return "";
    }

    protected function getParentChildrenStructure($elements)
    {
        $data = [
            'rows'      => [],
            'children'  => [],
            'errors'    => []
        ];

        foreach ($elements as $index => $row) {
            $p = $row['parent_id'];
            $u = $row['unique_id'];

            $error = $this->validData($data['rows'], $u, $p);

            if ($error != "") {
                $data['errors'][] = $error;
            }

            if (!isset($data['children'][$p])) {
                $data['children'][$p] = [];
            }

            $data['children'][$p][] = $u;
            $data['rows'][$u] = $p;
        }

        return $data;
    }

    public function getParentAndDescendantsFromList($parentIds)
    {
        if (!is_array($parentIds) || empty($parentIds)) {
            return [];
        }

        $ids = [];

        foreach ($parentIds as $id) {
            if (is_numeric($id)) {
                $ids = array_merge($ids, $this->getParentAndDescendants($id));
            }
        }

        return array_unique($ids);
    }   

    public function getParentAndDescendants($parentId)
    {
        $id = (int)$parentId;

        if (!isset($this->nodes[$id])) {
            return [];
        }

        $ids = $this->decodeIds($parentId);

        $parentAndDescendants = array_merge($ids[1], $ids[2]);

        if (!empty($id)) {
            $parentAndDescendants[] = (String)$id;
        }

        return $parentAndDescendants;
    }

    protected function addParentId($parentId, $ids)
    {
        $parentId = $this->decodeIds($parentId)[0][0];

        if ($parentId > 0) {
            $ids[] = $parentId;
            return $this->addParentId($parentId, $ids);
        }

        return $ids;
    }

}
