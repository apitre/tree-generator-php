<?php

class TreeNode
{
    public $value = 0;

    public function __construct($value)
    {
        $this->value = $value;
    }
}
